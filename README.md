# @mvik/asciidoctor-hill-chart

Create hill charts in Antora.

## Installation

`npm install @mvik/asciidoctor-hill-chart`

## Configuration

<p class="codeblock-label">playbook.yml</p>

```yml
asciidoc:
  extensions:
    - '@mvik/asciidoctor-hill-chart'
```

## Usage

```asciidoc
[hill-chart,width=500px]
.Tasks
--
Getting started;0.1
Almost there;0.64;red
Soon done;0.9;steelblue;5
--
```
