'use strict'

const hillchart = require('@mvik/hill-chart')
const rusha = require('rusha')
const path = require('path')

const getDirPath = (doc) => {
    const baseDir = doc.getBaseDir()
    const imagesDir = doc.getAttribute('imagesdir')
    return path.join(baseDir, imagesDir)
}

module.exports.register = function register(registry, context = {}) {

    registry.block(function () {
        const self = this
        self.named('hill-chart')
        self.onContext(['paragraph', 'listing', 'literal', 'open', 'example'])
        self.positionalAttributes(['target', 'format'])
        self.process((parent, reader, attrs) => {

            const doc = parent.getDocument()

            const format = attrs.format || doc.getAttribute('hill-chart-default-format') || 'svg'
            const blockId = attrs.id
            const caption = attrs.caption
            const title = attrs.title

            let hillChartText = reader.$read();
            const subs = attrs.subs
            if (subs) {
                hillChartText = parent.applySubstitutions(hillChartText, parent.$resolve_subs(subs))
            }

            const diagramName = `hc-${rusha.createHash().update(title + hillChartText).digest('hex')}.${format}`

            const {component, version, module} = context.file.src

            if (!context.contentCatalog.getById({component, version, module, family: 'image', relative: diagramName})) {
                const tasks = hillchart.createTasksFromCsv(hillChartText)
                const chartTitle = attrs.chartTitle || attrs.hillChartTitle
                const hill = hillchart.createHill({title: chartTitle || ''})
                const imageContents = hillchart.drawHillChart({tasks, hill}, format)
                const dirPath = getDirPath(doc)

                context.contentCatalog.addFile({
                    contents: imageContents,
                    src: {
                        component,
                        version,
                        module,
                        family: 'image',
                        mediaType: format === 'svg' ? 'image/svg+xml' : 'image/png',
                        path: path.join(dirPath, diagramName),
                        basename: diagramName,
                        relative: diagramName
                    }
                })
            }

            const blockAttrs = Object.assign({}, attrs)
            delete blockAttrs.title
            delete blockAttrs.caption
            delete blockAttrs.opts

            blockAttrs.target = diagramName
            blockAttrs.alt = 'Diagram'
            blockAttrs.width = attrs.width || '800px'
            if (blockId) {
                blockAttrs.id = blockId
            }
            const block = self.createImageBlock(parent, blockAttrs);
            if (title) {
                block['$title='](title)
            }
            block.$assign_caption(caption, 'figure')

            return block
        })
    })

    return registry
}
